-----------------------------------------------
-- INSERTING RECORDS---
----------------------------------------------
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");
----------------------
INSERT INTO albums(album_title, date_released, artist_id) VALUES (
    "Psy 6", "2022-1-1", 3
);
INSERT INTO albums(album_title, date_released, artist_id) VALUES (
    "Trip", "1996-1-1", 1
);
-----------
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Gangnam Style",
    253,
    "K-pop",
    1
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Kundiman",
    234,
    "OPM",
    2
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Kisapmata",
    259,
    "OPM",
    2
);
------------------------------------------
-- SELECTING RECORDS---
------------------------------------------
-- (*) to select all columns
SELECT * FROM songs;
-- to select specified columns
SELECT song_name, genre FROM songs;

-- display title of all OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";
-- use AND and OR keywords for mutiple expressions in the WHERE clause:
--e.g display title and length of OPM songs that are more than 2 mins. and 40 secs.
SELECT song_name, length FROM songs WHERE length > 240 AND genre = 'OPM';

------------------------------------------
-- UPDATING RECORDS---
------------------------------------------

UPDATE songs SET length = 220 WHERE song_name = "Kundiman";
UPDATE songs SET length = 220, song_name = "new name"  WHERE ...;

------------------------------------------
-- DELETING RECORDS---
------------------------------------------
DELETE FROM songs WHERE genre = "OPM" AND length > 240;