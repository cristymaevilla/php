------------------------------------------
-- ADD NEW RECORDS---
------------------------------------------
INSERT INTO artists (name) VALUES("Taylor Swift");
INSERT INTO artists (name) VALUES("Lady Gaga");
INSERT INTO artists (name) VALUES("Justin Beiber");
INSERT INTO artists (name) VALUES("Ariana Grande");
INSERT INTO artists (name) VALUES("Bruno Mars");
--add albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES(
    "Red",
    "2022-01-01",
    4
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES(
    "A Star is Born",
    "2022-01-01",
    5
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES(
    "Born Ths Way",
    "2022-01-01",
    5
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES(
    "Believe",
    "2022-01-01",
    6
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES(
    "Dangerous Woman",
    "2022-01-01",
    7
);
INSERT INTO albums (album_title, date_released, artist_id) VALUES(
    "Purpose",
    "2022-01-01",
    6
);