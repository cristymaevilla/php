<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php session_start(); ?>
    <h1>Add Task</h1>
    <form method="POST" action="./server.php"> 
        <input type="hidden" name= "action"
        value ="add">
        Description : <input type="text" name= "description" required>
        <button type= "submit">Add</button>
    </form>

    <h1>Task List</h1>
    <!-- isset checks if session array exists -->
    <?php if(isset($_SESSION['tasks'])): ?>
        <?php foreach($_SESSION['tasks']as $index => $task): ?>
        <div>
            <form action="./server.php" method="POST"  style="display: inline-block;">
                <input type="hidden" name= "action" value ="update">
                <input type="hidden" name= "id" value ="<?= $index?>">
                <input type="checkbox" name= "isFinished" <?=($task-> isFinished) ? "checked" : null ?>>
                <input type="text" name="description" value="<?=$task -> description ?>">
                <button type="submit">Update</button>
            </form>

            <form action="./server.php" method="POST"  style="display: inline-block;">
                <input type="hidden" name= "action" value ="remove">
                <input type="hidden" name= "id" value ="<?= $index?>">
                <button type="submit">Delete</button>
            </form>

        </div>
        <?php endforeach; ?>
        <?php endif; ?>
    
</body>
</html>