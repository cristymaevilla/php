<?php
session_start();

class Tasklist{
    public function add($description){
        $newTask = (object)[
            'description' => $description,
            'isFinished' => false
        ];
        if($_SESSION['tasks'] === null){
            $_SESSION['tasks'] = array();
        }
        array_push($_SESSION['tasks'], $newTask);
    }
    public function update($id, $description, $isFinished){ 
        // change the description of specific task
        $_SESSION['tasks'][$id]-> description = $description;
        
        $_SESSION['tasks'][$id]->isFinished = ($isFinished !== null)? true : false;
    }
    public function remove($id){
        // array_splace(array, index, number to omit)
        array_splice($_SESSION['tasks'],$id,1);
    }
}
$taskList = new TaskList();
 if($_POST['action']  === 'add'){
    //  if the action input's value is add, call add method
     $taskList->add($_POST['description']);
 }else if($_POST['action'] === 'update'){
     $taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
 }elseif($_POST['action'] === 'remove'){
    $taskList->remove($_POST['id']);
 }

//  redirect the user back to index
header('Location: ./index.php');