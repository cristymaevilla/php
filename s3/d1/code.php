<?php

/* ------------------------------------
   |      OBJECTS AS VARIABLES         |
   ------------------------------------
*/ 
$buildingObj = (object)[
   'name' => 'Caswyn Bldg.',
   'floor'=> 8,
   'address' => (object)[
      'barangay' => 'Sacred Heart',
      'city' => 'Quezon City',
      'country' => 'Philippines'
   ]
   ];
/* ------------------------------------
   |      OBJECTS AS CLASSES         |
   ------------------------------------
*/ 
class Building {
   public $name;
   public $floors;
   public $address;

   // constructor -used in creation of an object to provide the initial values of each property
   public function __construct($name, $floors, $address){
   $this->name = $name;
   $this->floors = $floors;
   $this->address = $address;
   }
   // methods
   public function printName(){
      return "The name of the building id $this->name";
   }
}
/* ------------------------------------
   |  INHERITANCE AND POLYMORPHISM   |
   ------------------------------------
*/
// below will inherit name floors and addres from Building class including function
class Condominium extends Building{
   // calling construct here will change the entire constructor:
   /*public function __construct($name, $floors, $address, $color){
      $this->name = $name;
      $this->floors = $floors;
      $this->address = $address;
      $this->color = $color;
      } */
   public function printName()
   {
      return "The name of this condominium is $this->name";
   }

}
 
$building = new Building('Caswyn  Building', 8 , 'Timog Avenue, Quezon City Philippines');

$building2 = new Building('Criz  Building', 1000 , 'Timog Avenue, Quezon City Philippines');

$condominium = new Condominium('V Condo', 6 , 'Villa Avenue, Makati City Philippines');