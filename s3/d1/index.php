<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S3</title>
    <style>
    </style>
</head>
<body>

    <h1>Objects from Variables</h1>
    <p><?php var_dump($buildingObj) ?></p>

    <h1>Objects from Classes</h1>
    <p><?php var_dump($building) ?></p>
    <p><?php var_dump($building2) ?></p>
    <h1>Inheritance</h1>
    <p><?= $building->printName(); ?></p>
    <p><?= $building2->printName(); ?></p>
    <h1>Polymorphism</h1>
    <p><?= $condominium->printName(); ?></p>






    
</body>
</html>