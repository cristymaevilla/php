<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S3: ACTTIVITY 1</title>
    <style>
        body{
            display:flex;
            justify-content : center;
        }
        div {
            border : 1px solid black ;
            padding : 20px;
            margin: 20px 100px 20px 100px;
            width : 500px;
            height : 350px;
    }
    </style>
</head>
<body>
    <div>
    <h1>Person</h1>
    <p><?= $person->printName(); ?></p>
    <h1>Developer</h1>
    <p><?= $developer->printName(); ?></p>
    <h1>Engineer</h1>
    <p><?= $engineer->printName(); ?></p>
    </div>
    
</body>
</html>