<?php
class Person {
    public $firstName;
    public $middleName;
    public $lastName;
 
    // constructor -used in creation of an object to provide the initial values of each property
    public function __construct($firstName, $middleName, $lastName){
    $this->firstName = $firstName;
    $this->middleName = $middleName;
    $this->lastName = $lastName;
    }
    // methods
    public function printName(){
       return "Your fullname is $this->firstName $this->middleName $this->lastName.";
    }
 }
 class Developer extends Person{
    public function printName()
    {
        return "Your fullname is $this->firstName $this->middleName $this->lastName and you are a developer.";
    }
 
 }
 class Engineer extends Person{
    public function printName()
    {
        return "You are an engineer $this->firstName $this->middleName $this->lastName.";
    }
 
 }
 $person = new Person('Senku','Kishin','Ishigami');
 $developer = new Developer('John', 'Finch' , 'Smith');
 $engineer = new Engineer('Harold', 'Myers' , 'Reese');