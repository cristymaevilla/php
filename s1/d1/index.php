<!-- import php file :  -->
<?php require_once "./code.php" ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC S01</title>
</head>
<body>
    <!-- use double qoutes -->
    <h1>VARIABLES:</h1>
    <p>
        <?php echo "Good day $name! Your email is $email."; ?>
    </p>
     <p> 
         <?php echo PI; ?>
    </p>
     <p> 
         <?php echo $address; ?>
    </p>
    <h1>DATA TYPES:</h1>
    <p> 
         <?php echo gettype($spouse); ?>
    </p>
    <p> 
         <?php echo var_dump($hasTravelledAbroad); ?>
    </p>
    <p> 
         <?php echo print_r($grades), var_dump($grades);?>
    </p>
    <p> 
         <?php echo $gradesObj -> firstGrading;?>
    </p>
    <p> 
         <!-- accessing object property -->
         <?php echo $personObj -> address -> state;?>
    </p>
    <p> 
         <?php echo var_dump($personObj); ?>
    </p>
    <p> 
        <!-- accessing element in an array -->
         <?php echo $grades[3]; ?>
    </p>

    <h1>OPERATORS:</h1>
    <h3>Arithmetic OPERATORS:</h3>
    <p> Sum: <?php echo $x + $y; ?></p>
    <p> Difference: <?php echo $x - $y; ?></p>
    <p> Product: <?php echo $x * $y; ?></p>
    <p> Qoutient: <?php echo $x / $y; ?></p>

    <h3>Equality OPERATORS:</h3>
    <p> Loose Equality / Inequality: <?php echo var_dump($x == $y); ?></p>
    <p> Strict Equality / Inequality: <?php echo var_dump($x == $y); ?></p>

    <h3>Greater/Lesser OPERATORS:</h3>
    <p> Is Lesser | greater : <?php echo var_dump($x < $y); ?></p>
    <p> Is Lesser | greater | equal to: <?php echo var_dump($x <= $y); ?></p>

    <h3>Logical OPERATORS:</h3>
    <p> Are ALL requirements met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
    <p> Are SOME requirements met: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
    <p> Are SOME requirements NOT met: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>

    <h1>Function:</h1>
    <p> Full Name :  <?php echo getFullName("John", "J", "Smith");  ?></p>

    <h1>Selection control structures:</h1>
    <p> <?php echo determineTyphoonIntensity(100); ?></p>

    <h3>Ternary Operator:</h3>
    <p> <?php var_dump(isUnderage(78)); ?></p>
    <p> <?php var_dump(isUnderage(13)); ?></p>

    <h3>Switch Statement:</h3>
    <p> <?php echo determineUser(2); ?></p>
</body>
</html>