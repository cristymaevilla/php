<?php
/* - php tag 
 to close the tag:
?> it is optional when ONLY writing PHP code*/

// echo - is used to output data to the screen
// NOTE always end with (;) in PHP code

/* ------------------------------------
   |        VARIABLES                 |
   ------------------------------------
*/ 

$name = 'John Smith';
$email = 'johnsmith@mail.com';

// echo $name;
// echo $email;

/* ---------------------------------
   |      CONSTANTS                |
   ---------------------------------
*/ 

define('PI', 3.1416);
// echo PI;
/* -------------------------------------
   |          DATA TYPES               |
   -------------------------------------
*/ 
// STRINGS:
$state = "new York";
$country = "USA";
// CONCATINATION:
$address = $state . ',' . $country; // using (.)
$address2 = " $tate, $country"; // double qoutes

// INTEGERS:
$age = 31;
$headcount = 26;

// FLOATS:
$grade = 98.2;
$distance = 1342.12;

// BOOLEANS:
$hasTravelledAbroad = true;
$spouse = null;
// normal echoing of boolean and null will not be visible. To see their types, use: gettype(); | var_dump();

// ARRAY

$grades = array(98.7 , 92.1, 96.5, 90.5);

// OBJECTS:
$gradesObj = (object) [
    'firstGrading' => 98.7,
    'seconfGrading' => 92.1,
    'thirdGrading' => 96.5,
    'forthGrading' => 94.6
];

$personObj = (object)[
    'fullName' => "John Smith",
    'isMarried' => false,
    'age' => 53,
    'address' => (object)[
        'state' => "New York",
        'country' => "USA"
    ]
];
/* -----------------------------------
   |         OPERATORS               |
   -----------------------------------
*/ 
// ASSIGNMENT OPERATOR (=)
$x = 234;
$y = 123;
$isLegalAge = true;
$isRegistered = false;
/* -----------------------------------
   |         FUNCTIONS               |
   -----------------------------------
*/ 
function getFullName($firstName, $middleInitial, $lastName){
 return "$lastName, $firstName $middleInitial";
};
/* -----------------------------------
   | SELECTION CONTROL STRUCTURES    |
   -----------------------------------
*/
function determineTyphoonIntensity($windSpeed){
   if($windSpeed < 30){
      return "Not a typhoon.";
   }else if($windSpeed <= 61){
      return "Tropical depression detected.";
   }else if($windSpeed >= 62 && $windSpeed <= 88){
      return "Tropical storm detected";
   }else if($windSpeed >= 89 && $windSpeed <= 177){
      return "Severe tropical storn detected";
   }else{
      return "Typhoon detected";
   }
};

/* -----------------------------------
   |       TERNARY OPRATOR        |
   -----------------------------------
*/
function isUnderAge($age){
   return ($age < 18)? true : false;
}
/* -----------------------------------
   |       SWITCH STATEMENT         |
   -----------------------------------
*/
function determineUser($computerNumber){
   switch($computerNumber){
      case 1: 
         return 'Linus Torvalds';
         break;
      case 2: 
         return 'Steve Jobs';
         break;
      case 3: 
         return 'Sid Meier';
         break;
      default :
         return "The computer numberb $computerNumber is out of bounds";
         break;
   }
}