<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ACTTIVITY 1</title>
    <style>
        div {border : 1px solid black ;
            padding : 20px;
            /* display: flex;
        flex-direction: column;
        align-items:middle;
        justify-content: center; */
    }
    </style>
</head>
<body>
    <div >
    <h1>Full Address</h1>
    <p>
        <?php echo getFullAddress( "Philippines", "Quezon City",  "Metro Manila", "Timog Avenue", "3F Caswyn Bldg." ); ?>
    </p>
    <p>
        <?php echo getFullAddress( "Philippines", "Makati City",  "Metro Manila", "Buendia Avenue", "3F Enzo Bldg." ); ?>
    </p>
    </div>

    <div >
    <h1>Letter-Based Grading</h1>
    <p><?php echo getLetterGrade(87); ?></p>
    <p><?php echo getLetterGrade(94); ?></p>
    <p><?php echo getLetterGrade(74); ?></p>
    </div>

    
</body>
</html>