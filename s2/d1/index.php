<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC S02</title>
</head>
<body>
    <h1>Repetition Control Structures</h1>
    <h3>While Loop</h3>
    <?php whileLoop(); ?>
    <h3>Do While Loop</h3>
    <?php doWhileLoop(); ?>
    <h3>For Loop</h3>
    <?php forLoop(); ?>

    <h1>Array Manipulation : </h1>
    <h2>Types of Array</h2>
    <h3>Simple Array</h3>
    <ul>
    <?php forEach($grades as $grade){ ?>
        <li><?php echo $grade; ?></li>
        <?php
    } ?>
    </ul>
    <h3>Associative Array</h3>
    <ul>
      
    <?php forEach($gradePeriods as $period => $grade){ ?> 
        <li>Grade in <?= $period;//<?= is the shortcut for echo ?> is <?= $grade; ?></li>
        <?php } ?>
    </ul>
    
    <h3>Multidimensional Array</h3>
    <ul>
        <?php 
        forEach($heroes as $team){
            forEach($team as $member){
                ?>
                <li> <?= $member; ?></li>
                <?php
            }
        }
        ?>
        <?php
        for($i = 0; $i < count($heroes); $i++){
            for($j= 0; $j < count($heroes[$i]); $j++){
                echo $heroes[$i][$j] ?> </br>
                <?php
            }
        }
        ?>
    </ul>

    <h3>MULTIDIMENSIONAL ASSOCIATIVE ARRAY</h3>
    <?php
    forEach($powers as $label => $powerGroup){
        forEach($powerGroup as $power){
            ?>
            <li><?= "$label: $power"; ?></li>
            <?php
        }
    } ?>

    <h1>ARRAY FUNCTIONS</h1>
    <h3>Add to Array:</h3>
    <?php array_push($computerBrands, 'Apple'); ?>
    <pre><?php print_r($computerBrands); ?></pre>
    <?php array_unshift($computerBrands, 'Dell'); ?>
    <pre><?php print_r($computerBrands); ?></pre>

    <h3>Remove Array</h3>
    <?php array_pop($computerBrands); ?>
    <pre><?php print_r($computerBrands); ?></pre>
    <?php array_shift($computerBrands); ?>
    <pre><?php print_r($computerBrands); ?></pre>

    <h3>Sort Reverse</h3>
    <?php sort($computerBrands); ?>
    <pre><?php print_r($computerBrands); ?></pre>
    <?php rsort($computerBrands); ?>
    <pre><?php print_r($computerBrands); ?></pre>

    <h3>Count Array Elements</h3>
    <p><?= count($computerBrands) ?></p>

    <h3>In Array</h3>
    <p><?= searchBrand($computerBrands, 'Asus'); ?></p>
    <p><?= searchBrand($computerBrands, 'Fujitsu'); ?></p>
</body>
</html>

