<?php

/* ------------------------------------
   |       REPETITION               |
   ------------------------------------
*/ 
/* WHILE LOOP- takes a single condition. For as long as the condition is true*/
function whileLoop(){
    $count = 0;
    while($count <= 5){
        echo $count . '</br>';
        $count++;
    }
}
/* DO WHILE LOOP- guaranteed to run at least once before the checking*/
function doWhileLoop(){
    $count =7;
    do{
        echo $count . '</br>';
        $count--;
    }while( $count > 0);
}
/* FOR LOOP- 3 parts:
-initaila value to track the progression of the loop
-condition that will be evaluated and will determine whether the loop will continue or not
-the iteration method*/
function forLoop(){
    for( $i =0; $i <= 4; $i++){
        echo $i . "</br>";
    }
}
/* ------------------------------------
   |      ARRAY MANIPULATION          |
   ------------------------------------
*/ 
$studentNumbers =array( '1993', '1897','1879');
$grades = [78, 89, 65, 90];
// SIMPLE ARRAY: 
$years =['1993', '1897','1879']; 

// ASSOCIATIVE ARRAY:
$gradePeriods = ['firstGrading' => 98.5, 'seconfGrading' => 94.3, 'thirdGrading' => 89.3, 'fourthGrading' => 90.2];

// MULTIDIMENSIONAL ARRAY:
$heroes = [
    ['Iron Man', 'Thor', 'Hulk'],
    ['Wolverine', 'Cyclops', 'Jean Grey'],
    ['Batman', 'Superman', 'Wonder Woman']
];
// index of Batman: [2][0]
// Iron Man : [0],[0]
// MULTIDIMENSIONAL ASSOCIATIVE ARRAY:
$powers = [
    'regular' => ['Rocket Punch', 'Laser eyes'],
    'signature' => [
        'i', 'dont', 'know'
    ]
    ];

$computerBrands = ['Asus', 'Dell', 'Lenovo'];
// Searching Arrays:
function searchBrand($brandsArr, $brand){
    if(in_array( $brand, $brandsArr)){
        return "$brand is in the array";

    }else{
        return "$brand is not in the Array";
    }
}