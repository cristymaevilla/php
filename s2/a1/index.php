<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S2: ACTTIVITY 1</title>
    <style>
        div {border : 1px solid black ;
            padding : 20px;
            margin: 20px 30px 20px 30px;
    }
    </style>
</head>
<body>
    <div>
    <h2>Divisibles of Five</h2>
    <?php Numbers(); ?>
    </div>

    <div>
    <h2>Array Manipulation</h2>

    <?php array_push($names, 'John Smith'); ?>
    <p><?php var_dump($names); ?></p>
    <p><?= count($names); ?></p>

    <?php array_push($names, 'Jane Smith'); ?>
    <p><?php var_dump($names); ?></p>
    <p><?= count($names); ?></p>

    <?php array_shift($names); ?>
    <p><?php var_dump($names); ?></p>
    <p><?= count($names); ?></p>

    </div>


    
</body>
</html>