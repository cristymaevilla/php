------------------------------------------
-- ADD NEW RECORDS---
------------------------------------------
INSERT INTO artists (name) VALUES("Taylor Swift");
INSERT INTO artists (name) VALUES("Lady Gaga");
INSERT INTO artists (name) VALUES("Justin Beiber");
INSERT INTO artists (name) VALUES("Ariana Grande");
INSERT INTO artists (name) VALUES("Bruno Mars");
--add albums
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
    "Fearless",
    246,
    "Pop Rock",
    3
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
    "Love Story",
    213,
    "Country Pop",
    3
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
    "Red",
    204,
    "Country",
    4
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
    "State of Grace",
    253,
    "Rock",
    4
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
    "Born This Way",
    252,
    "Electropop",
    6
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
    "Black Eyes",
    151,
    "Rock",
    6
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
    "Sorry",
    152,
    "Dancehall-op",
    7
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
    "Into You",
    242,
    "EDM",
    7
);

SELECT * FROM songs WHERE id = 1 OR song_name = "Into you"; 
--specific ids
SELECT * FROM songs WHERE id IN (1, 5, 6);
--end of string
SELECT * FROM songs WHERE song_name LIKE "%ss";
--beginning
SELECT * FROM songs WHERE song_name LIKE "b%";
--search entire string
SELECT * FROM songs WHERE song_name LIKE "%a%";
--order
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;
--limit
SELECT * FROM songs ORDER BY song_name ASC LIMIT 3;

SELECT DISTINCT genre FROM songs;
SELECT COUNT(*) FROM songs;

---------------------
--  TABLE JOINS ----
-------------------_-
--https://joins.spathon.com/
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists INNER JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists LEFT JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists RIGHT JOIN albums ON artists.id = albums.artist_id;

SELECT albums.album_title, artists.name FROM artists RIGHT JOIN albums ON  albums.artist_id = artists.id;

--MULTIPLE JOIN
SELECT artists.name, albums.album_title, songs.song_name  FROM artists
    LEFT JOIN albums ON  artists.id = albums.artist_id
    LEFT JOIN songs ON  albums.id = songs.album_id;

