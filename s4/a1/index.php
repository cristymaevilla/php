<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S4: ACTTIVITY 1</title>
    <style>
        *{
            box-sizing: border-box;
        }
        body{
            width: 98%;
            height: 100%;
            display:flex;
            justify-content: center;
            flex-direction: column;
        }
        div {border : 1px solid black ;
            padding : 20px;
            margin: auto;
            width: 800px;
            align-self: center;
    }
    </style>
</head>
<body>
    <div>
    <h1>Building</h1>
    <p>The name of the building is
        <?= $building->getName(); ?>.
    </p>

    <p>The <?= $building->getName(); ?> has
        <?= $building->getFloors(); ?> floors.
    </p> 

    <p> The <?= $building->getName(); ?> is located at
        <?= $building->getAddress(); ?>.
    </p>

    <?php $building->setName('Caswyn Complex'); ?>
    <p>The name of the condominum has been changed to
        <?= $building->getName(); ?>.
    </p>


    <h1>Condominium</h1>
    <p>The name of the building is
        <?= $condominium->getName(); ?>.
    </p>

    <p>The <?= $condominium->getName(); ?> has
        <?= $condominium->getFloors(); ?> floors.
    </p> 

    <p> The <?= $condominium->getName(); ?> is located at
        <?= $condominium->getAddress(); ?>.
    </p>

    <?php $condominium->setName('Enzo Tower'); ?>
    <p>The name of the condominum has been changed to
        <?= $condominium->getName(); ?>.
    </p>
    
    </div>


    
</body>
</html>