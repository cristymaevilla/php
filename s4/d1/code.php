<?php

/* ------------------------------------
   |        ACCESS MODIFIERS          |
   ------------------------------------
-each property and methid inside of class can be given a certain access modifier

-public means that the property/method is accessib;e to all and can be reassined/changed by anyone

-private means thet direct access to an object's property is disabled and can't be changed, inheritance is disabled

-protected disables direct acces to objects properties and methods but inheritance is allowed.. protected access modifier is laso inherited to the child class.
   */ 
class Building {

   protected $name;
   protected $floors;
   protected $address;

   public function __construct($name, $floors, $address){
      $this-> name = $name;
      $this-> floors = $floors;
      $this-> address = $address;
   }
   


}
// ENCAPSULATION:
// using getter and setter methods we can implement encapsulation of an object's data
// getters and setters serve as an intermediary in accesssing or reassignning an objects properties and methods, work bith private and protected
// no need to have getter and setter in very property
class Condominium extends Building{
      public function getName(){
         return $this->name;
      }
      public function setName($name){
         $this->name=$name;
      }
}
$building = new Building('Caswyn', 8, 'T Ave., Quezon City, Phils' );
$condominium = new Condominium('V Condo', 99, 'V Ave., Makati City, Phils.' );