<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S4</title>
    <style>
    </style>
</head>
<body>

    <h1>Access Modifiers</h1>
    <h3></h3>
    <!-- <//?php $building->name ="Changed Name"; ?>
    <p></?= $building->name;?></p> -->

    <h3>Condominium</h3>
    <!-- <p></?= $condominium->name;?></p> -->

    <h1>Encapsulation</h1>
    <?php $condominium->setname('Enzo Tower'); ?>
    <?= $condominium->getName(); ?>




    
</body>
</html>