<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class WelcomeController extends Controller
{
    public function feature()
    {
            $allposts = Post::all();
            $posts = array();    
            if(count($allposts) > 2) {
                $posts = Post::all()->random(3);
            }else{
                $posts = $allposts;
            }
            return view('welcome')->with('posts', $posts);
    }


}
