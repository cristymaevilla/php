<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\WelcomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
 Route::get('/', [WelcomeController::class, 'feature']);

// Route::get('/', [PostController::class, 'welcome']);

//route to return a view where the user can create a post
Route::get('/posts/create', [PostController::class, 'create']);

//route for a route wherein form data can be sent via POST method
Route::post('/posts', [PostController::class, 'store']);

//route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

//route that will return a view of  all posts og the logged user
Route::get('/posts/myPosts', [PostController::class, 'myPosts']);

//route that will view a specific post
Route::get('/posts/{id}', [PostController::class, 'show']);

//route that will view a form that will edit a post when the post title is clicked
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

Route::put('/posts/{id}', [PostController::class, 'update']);

Route::delete('/posts/{id}', [PostController::class, 'archive']);

//route that will enable users to like/unlike posts
Route::put('/posts/{id}/like', [PostController::class, 'like']);

//route that will enable users to like/unlike posts
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);


Auth::routes();

Route::get('/home', [HomeController::class, 'index']);
